resource "aws_lb" "web_lb" {
    load_balancer_type = "application"

    name = "astro-load-balancer"

    internal = false

    security_groups = [ 
        aws_security_group.astro_elb_sc.id
    ]
    subnets = aws_subnet.public_subnets.*.id

    depends_on = [
        aws_subnet.public_subnets,
        aws_security_group.astro_elb_sc
    ]
}

resource "aws_lb_target_group" "alb_http" {
    name = "alb-http"

    vpc_id = aws_vpc.astro_vpc.id

    port = "80"

    protocol = "HTTP"

    health_check {
      path = "/"
      port = "80"
      protocol = "HTTP"
      healthy_threshold = 5
      unhealthy_threshold = 2
      interval = 5
      timeout = 4
      matcher = "200"
    }

    depends_on = [
      aws_vpc.astro_vpc
    ]
}

resource "aws_lb_target_group_attachment" "alb_targetgroup_att" {
    target_group_arn = aws_lb_target_group.alb_http.arn

    count = length(var.public_subnet_cidr)

    port = 80

    target_id = element(aws_instance.PublicEC2.*.id, count.index)
}

resource "aws_lb_listener" "web" {
  load_balancer_arn = aws_lb.web_lb.arn

  port = 80

  protocol = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.alb_http.arn
  }

  depends_on = [
    aws_lb.web_lb,
    aws_lb_target_group.alb_http
  ]
}