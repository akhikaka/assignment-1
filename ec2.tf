resource "aws_instance" "PublicEC2" {
    count = length(var.public_subnet_cidr)
    ami = var.ami
    instance_type = var.instance_type

    vpc_security_group_ids = [
        aws_security_group.astro_public_sc.id
        ]

    subnet_id = aws_subnet.public_subnets[count.index].id

    key_name = "astro_key"

    tags = {
        Name = format("PublicEC2-%d", count.index+1)
    }

    user_data = <<-EOF
                    #!/bin/bash
                    yum update -y
                    yum install -y httpd
                    systemctl start httpd.service
                    systemctl enable httpd.service
                    echo "Well done PUBLIC $(hostname -f) Terraform" > /var/www/html/index.html
                    EOF
    
    iam_instance_profile = aws_iam_instance_profile.ec2-role.name

    depends_on = [
        aws_vpc.astro_vpc,
        aws_subnet.public_subnets,
        aws_security_group.astro_public_sc,
        aws_key_pair.astro_key
        ]
}

resource "aws_instance" "PrivateEC2" {
    count = length(var.private_subnet_cidr)
    ami =   var.ami
    instance_type = var.instance_type

    vpc_security_group_ids = [
        aws_security_group.astro_private_sc.id
        ]
    subnet_id = aws_subnet.private_subnets[count.index].id

    key_name = "astro_key"

    tags = {
        Name = format("PrivateEC2-%d", count.index+1)
    }

    user_data = <<-EOF
                        #!/bin/bash
                        yum update -y
                        yum install -y httpd
                        systemctl start httpd.service
                        systemctl enable httpd.service
                        echo "Ughhh PRIVATE $(hostname -f) Terraform" > /var/www/html/index.html
                        EOF
    
    iam_instance_profile = aws_iam_instance_profile.ec2-role.name
    
    depends_on = [
        aws_vpc.astro_vpc,
        aws_subnet.private_subnets,
        aws_security_group.astro_private_sc,
        aws_key_pair.astro_key
        ]
}