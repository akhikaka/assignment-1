resource "aws_vpc" "astro_vpc" {
    cidr_block = var.vpc_cidr
    instance_tenancy = "default"
    enable_dns_support = true

    tags = {
      "Name" = "vpc_astro"
    }
}

resource "aws_internet_gateway" "astro_ig" {
    vpc_id = aws_vpc.astro_vpc.id

    tags = {
      "Name" = "astro_ig"
    }

    depends_on = [
      aws_vpc.astro_vpc
    ]
  
}

resource "aws_eip" "astro_eip" {
    vpc = true
    tags = {
      "Name" = "astro_eip"
    }
}

resource "aws_nat_gateway" "astro_nat" {
    allocation_id = aws_eip.astro_eip.id
    subnet_id = aws_subnet.public_subnets[0].id

    tags = {
      "Name" = "Astro NAT"
    }

    depends_on = [
      aws_eip.astro_eip,
      aws_subnet.public_subnets
    ]
  
}