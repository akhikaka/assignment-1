resource "aws_security_group" "astro_public_sc" {
    name = "astro_public_sc"
    description = "Allow access from internet"
    vpc_id = aws_vpc.astro_vpc.id

    ingress {
      cidr_blocks = [ "0.0.0.0/0" ]
      protocol = "-1"
      from_port = 0
      to_port = 0
      security_groups = [
          aws_security_group.astro_elb_sc.id
        ]
    }

    egress {
      cidr_blocks = [ "0.0.0.0/0" ]
      protocol = "-1"
      from_port = 0
      to_port = 0
    }

    tags = {
      "Name" = "astro_public_sc"
    }

    depends_on = [
      aws_vpc.astro_vpc,
      aws_security_group.astro_elb_sc
    ]
  
}

resource "aws_security_group" "astro_private_sc" {
    name = "astro_private_sc"
    description = "Allow "
    vpc_id = aws_vpc.astro_vpc.id

    ingress {
      protocol = "-1"
      from_port = 0
      to_port = 0
      security_groups = [
          aws_security_group.astro_public_sc.id
      ]
    }

    egress {
      cidr_blocks = [ "0.0.0.0/0" ]
      protocol = "-1"
      from_port = 0
      to_port = 0
    }

    tags = {
      "Name" = "astro_private_sc"
    }

    depends_on = [
      aws_vpc.astro_vpc,
      aws_security_group.astro_public_sc
    ]
  
}

resource "aws_security_group" "astro_elb_sc" {
    name = "astro_elb_sc"
    description = "Allow ELB access from internet"
    vpc_id = aws_vpc.astro_vpc.id


    ingress {
      cidr_blocks = [ "0.0.0.0/0" ]
      protocol = "tcp"
      from_port = 80
      to_port = 80
    }

    egress {
      cidr_blocks = [ "0.0.0.0/0" ]
      protocol = "-1"
      from_port = 0
      to_port = 0
    }

    tags = {
      "Name" = "astro_elb_sc"
    }
  
}