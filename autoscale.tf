resource "aws_launch_configuration" "worker" {
    name_prefix = "autoscaled-astro-"
    image_id = var.ami
    instance_type = var.instance_type
    security_groups = [ 
      aws_security_group.astro_elb_sc.id 
     ]
    associate_public_ip_address = true
    key_name = "astro_key"
    user_data = <<-EOF
                    #!/bin/bash
                    yum update -y
                    yum install -y httpd
                    systemctl start httpd.service
                    systemctl enable httpd.service
                    echo "Well done PUBLIC $(hostname -f) AUTOSCALE" > /var/www/html/index.html
                    EOF
    lifecycle {
     create_before_destroy = true 
    }
}

resource "aws_autoscaling_group" "worker" {
  name = "my_asgroup_${aws_launch_configuration.worker.name}"
  min_size = 1
  desired_capacity = 1
  max_size = 3
  vpc_zone_identifier = aws_subnet.public_subnets.*.id
  launch_configuration = aws_launch_configuration.worker.name
  health_check_type = "ELB"

  target_group_arns = [ aws_lb_target_group.alb_http.arn ]
  default_cooldown = 30
  health_check_grace_period = 30

  lifecycle {
    create_before_destroy = true
  }
}