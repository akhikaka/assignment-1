# STEPS
1. Modify region, access_key and secret_key in `providers.tf`
2. Modify variables in `env.tf` as needed
3. Run `terraform apply --auto-approve`