resource "aws_route_table" "PublicRouteTable" {
    vpc_id = aws_vpc.astro_vpc.id

    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.astro_ig.id
    }

    tags = {
      "Name" = "PublicRouteTable"
    }

    depends_on = [
      aws_vpc.astro_vpc,
      aws_internet_gateway.astro_ig
    ]
  
}

resource "aws_route_table_association" "public_routetable_assoc" {
    count = length(var.public_subnet_cidr)

    subnet_id = element(aws_subnet.public_subnets.*.id, count.index)

    route_table_id = aws_route_table.PublicRouteTable.id

    depends_on = [
      aws_route_table.PublicRouteTable,
      aws_subnet.public_subnets
    ]
  
}

resource "aws_route_table" "PrivateRouteTable" {
    vpc_id = aws_vpc.astro_vpc.id

    route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_nat_gateway.astro_nat.id
    }

    tags = {
      "Name" = "PrivateRouteTable"
    }

    depends_on = [
      aws_vpc.astro_vpc,
      aws_nat_gateway.astro_nat
    ]
  
}

resource "aws_route_table_association" "private_routetable_assoc" {
    count = length(var.private_subnet_cidr)

    subnet_id = element(aws_subnet.private_subnets.*.id, count.index)

    route_table_id = aws_route_table.PrivateRouteTable.id

    depends_on = [
      aws_route_table.PrivateRouteTable,
      aws_subnet.private_subnets
    ]
  
}