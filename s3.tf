resource "aws_s3_bucket" "astro-assignment-bucket" {
  bucket = "astro-assignment-bucket"
  acl    = "private"

  tags = {
    Name        = "astro-assignment-bucket"
  }
}